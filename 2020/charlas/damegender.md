# Damegender
 
The gender detection from a name is useful to make gender studies from
social networks, mailing lists, software repositories, articles,
etc. Nowadays there are various APIs to detect gender from a name. In
this speech, we offer a tool to use and compare these apis and a method
to predict gender applying machine learning and using a free license
and census open data.

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

This talk show you how to install the software, detect gender from
names from different sources and acquire mathematical resources to
compare methods (accuracies, confusion matrix, ...)

Who want to explain in detail the machine learning task about gender
detection as example of use for scikit and nltk. Very popular
libraries for artificial intelligence in Python.

## Público objetivo

People who likes Python, women who wants gender equality in science.

## Ponente(s)

David Arroyo Menéndez is doing the Phd in the URJC. He starts to
participate in research groups with e-learning publications at UNED,
while he is doing the degree in Computing Science.
Later, he was working in the private companies in the web programming
area. After of this, he starts master studies in the UCM to understand
the methods in the sociology. Later, he decides starts the Phd with
Jesús González Barahona by the ethical compromise with the Free
Software and the scientific interest measuring the phenomenon with
specialization in the gender matters.

### Contacto(s)

* Nombre: contacto

David Arroyo Menéndez
d.arroyome@alumnos.urjc.es

## Comentarios

http://www.damegender.net
https://github.com/davidam/damegender

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
