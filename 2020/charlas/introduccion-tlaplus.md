# Tu código apesta: Vamos a ponernos formales

Tu código apesta: Vamos a ponernos formales. Verificación formal de código con TLA+.

En esta charla se explicarán las ventajas de la verificación formal, junto a una breve introducción a TLA+.

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

Gran parte de los errores en aplicaciones, sean de funcionalidad o seguridad, proceden de fallos en el diseño: casuísticas que no se tienen en cuenta a la hora de elaborar los requisitos técnicos o de implementar el código. Este tipo de errores son difíciles de detectar con técnicas clásicas, especialmente en arquitecturas complejas, como puede ser la de una aplicación con procesos concurrentes.

La verificación formal de software permite determinar si el diseño de un algoritmo o sistema es lógicamente correcto, incluso antes de comenzar la implementación real. [TLA+](https://lamport.azurewebsites.net/tla/tla.html) es una pila tecnológica que permite la verificación formal, de [licencia libre](https://github.com/tlaplus/tlaplus/blob/master/LICENSE) y cuya comunidad de desarrollo está liderada por [Leslie Lamport](https://es.wikipedia.org/wiki/Leslie_Lamport). Gracias a PlusCal, el lenguaje de pseudocódigo de TLA+, se reduce la curva de aprendizaje permitiendo obtener rápidamente los beneficios de la verificación formal. 

En esta charla se explicarán las ventajas de la verificación formal, junto a una breve introducción a TLA+.

## Público objetivo

Cualquier persona interesada con conocimientos técnicos sobre programación.

## Ponente(s)

* Jesús Marín: Ciberseguridad/Desarrollo seguro. Líder del grupo local OWASP Almería. EA7KGH. Más información en [jesusmg.org](https://www.jesusmg.org).

### Contacto(s)

* Jesús Marín: contacto@jesusmg.org

## Comentarios

De ser necesario, podría hacerse en formato taller.

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/2020/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
