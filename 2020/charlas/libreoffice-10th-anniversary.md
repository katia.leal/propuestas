# Title of the proposal

LibreOffice 10th Anniversary: the many faces of a global FOSS community

## Proposal format

Choose one of these:

* [x] Talk (25 minutes)
* [ ] Lightning talk (10 minutes)

## Description

LibreOffice was announced in 2010 and will celebrate its 10th anniversary in 2020. During these 10 years, the global community has grown from a small group of volunteers - mostly based in Europe - to a large and diverse group of free software advocates active in most countries. The presentation will discuss the challenges faced by the community during its incredible growth over the past 10 years.

## Target audience

FOSS community members

## Speaker(s)

Italo Vignoli

Italo Vignoli is a founding member of The Document Foundation, the Chairman Emeritus of Associazione LibreItalia, an Emeritus Member of the Open Source Initiative (OSI) board, and co-chair of the ODF Advocacy OASIS Open Project. He co-leads LibreOffice marketing, PR and media relations, co-chairs the certification program, and is a spokesman for the project.

Speaker at many FOSS events since 2006, in Italia, English, French and Spanish

### Contact(s)

* Italo Vignoli: italo@libreoffice.org

## Observations

None

## Conditions

* [x] I agree to follow the [code of conduct](https://eslib.re/2019/conducta/) and request this acceptance from the attendees and speakers.
* [x] At least one person among those proposing will be present on the day scheduled for the talk.
