# Taller de Sistemas Distribuidos basados en Open Source

El principal objetivo del taller es obtener una visión global del
funcionamiento de los sistemas distribuidos. Se verá de forma práctica
como se usan algunas de las tecnologías (Open Source) como
Nomad/Kubernetes, Kafka, Spark o HDFS.

Los sistemas distribuidos nos acompañan desde 1960 y, a partir
de 1970, aplicaciones reales de ellos aparecieron: redes Ethernet,
sistemas de correo o ARPANET (el predecesor de Internet).

Pero, ¿por qué son tan relevantes los sistemas distribuidos actualmente?
Con la llegada de la computación en la nube y la necesidad del procesamiento
de datos masivos (big data) los sistemas distribuidos han pasado a ser
claves en estas aplicaciones que funcionan a nivel mundial. Miles de 
ordenadores con la capacidad de poder crecer se necesitan para 
satisfacer las demandas de estas aplicaciones.

Según podemos leer del Manifiesto Reactivo: «Las aplicaciones actuales se
despliegen en cualquier cosa desde dispositivos móviles a clusters basados
en la nube ejecutados por miles de procesadores multi-núcleo. Los usuarios
esperan tiempos de respuesta de milisegundos y 100% de disposibilidad. Los
datos se miden en Petabytes. Las demandas actuales, sencillamente, no estaban
acubiertos por las arquitecturas de software anteriores.»

En este taller las tecnologías claves necesarias para implementar estos 
sistemas distribuidos con Open Source serán presentadas, y algunas de ellas
se mostrarán de forma práctica: gestores de recursos, planificadores, motores
de ejecución, entornos de desarrollo o sistemas de almacenamiento.

## Objetivos a cubrir en el taller

El principal objetivo del taller es obtener una visión global del
funcionamiento de los sistemas distribuidos. Y de forma práctica,
se mostrará como se usan algunas de las tecnologías básicas Open Source.

En el taller práctico, se partirá de un pequeño cluster basados en
máquinas virtuales que será gestionado por Nomad y/o Kubernetes (
gestores de recursos y planificación) y sobre el que se ejecutará 
una ETL que leerá datos de forma distribuida, los publicrá en
Kafka y se procesarán de forma distribuida con Spark, dejando
los resultados en el sistema de ficheros distribuido HDFS. 
El objetivo final es tener escabalidad potencial infinita.

## Público objetivo

Todo desarrollador o administrador de sistemas que quieran entender
los requisitos que imponen el trabajar con sistemas distribuidos, en
especial aquellos perfiles que apuesten por devops.

## Ponente(s)

Alvaro del Castillo San Felix <adelcastillo@thingso2.com>

En la pasada edición de esLibre presentó el taller de Big Data que será
una de las partes incluidas dentro de este nuevo taller, que tiene objetivos
más ambiciosos.

### Contacto(s)

* Alvaro del Castillo San Felix: <adelcastillo@thingso2.com>

## Prerequisitos para los asistentes

Será necesario tener un equipo en el que se pueden desplegar contenedores docker,
siendo recomendable que esté basado en Linux.

## Prerequisitos para la organización

Para el taller será necesario disponer de un proyector y conexión a Internet.
Si además se dispone de ordenadores basados en Linux y con docker instalado,
mejor que mejor.

## Tiempo

El taller da pie a tener una duración flexible en función de cuanto se quiera
cubrir y con que necesidad. Con 2h debería de ser suficiente para una introducción.

## Día

Preferentemente el Viernes 5 de Junio.

## Comentarios


## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/).
* [X] Al menos una persona entre los que proponen el taller estará presente el día programado para el mismo.
* [X] Acepto coordinarme con la organización de esLibre.


