# Título de la propuesta

Crear un proyecto como Galiciacorre.es con herramientas libres

## Formato de la propuesta

Indicar uno de estos:

* [X] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

Si tenemos una idea de sitio/aplicación web, no tenemos porque recurrir a herramientas privativas. Podemos construir un proyecto con software/herramientas libres:
* SO
* Servidor Web
* API REST
* Vista Web
* Obtención de datos
* Almacenamiento de datos

El objetivo de la charla sería la descripción de las diferentes herramientas y software libre que se está usando en galiciacorre.es para demostrar que no es necesario acudir a herarmientas privativas para montarte algo funcional y usable.

Se describirán los componentes principales usados y se entrará en detalle en los más llamativos


## Público objetivo

Cualquier asistente en general, con un mínimo conocimiento del software libre y con interés en montarse algo por su cuenta o simplemenete que quiera escuchar a unos gallegos explicando qué es lo que han hecho.


## Ponente(s)

* Christian López: Drupal developer & lover. Cofundador de galiciacorre.es. Ingeniero Informático por la USC. Twitter: @christianlrcalo 
* Diego Carracedo: Drupal developer & frontend developer. Cofundador de galiciacorre.es. Ingeniero Informático por la USC. Twitter: @D_Carracedo

### Contacto(s)

* Christian López: clopezrodriguez95@gmail.com
* Diego Carracedo: dcarracedo@gmail.com

## Comentarios

De ser posible, aumentar el tiempo de la charla a 30 minutos para entrar algo en detalle en alguno de los puntos.

## Condiciones

* [X] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [X] Al menos una persona entre los que la proponen estará presente el día programado para la charla.
